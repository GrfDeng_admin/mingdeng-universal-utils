// webpack是基于nodejs的，需要使用CommonJS规范

const path = require('path')
module.exports = {
  /**
   * webpack执行入口
   */
  entry: './src/test.js',
  /**
   * 输出
   */
  output: {
    // 输出路径，必须是绝对路径
    path: path.resolve(__dirname, './../dist'),
    // 输出文件名
    filename: 'main.js'
  },
  /**
   * 模式
   */
  mode: 'production',
  /**
   * 模块
   */
  module: {
  }
}