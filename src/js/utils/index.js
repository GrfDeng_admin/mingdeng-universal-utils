
export {
  formatTime,
  deepClone,
  getObjType,
  delObjectValueNull,
  toQueryPair,
  timeDifference,
  fullscreenToggel,
  listenfullscreen,
  fullscreenEnable,
  reqFullScreen,
  exitFullScreen,
  randomLenNum,
  openWindow,
  formatDate,
  debounce,
  throttle,
  getQueryVariable,
  getQueryParams
}


/**
 * 时间格式化  刚刚 | x分钟前 | x小时前 | 1天前
 * @param {Date | Number | String} time 
 * @param {String | Null} fmt 格式 y:年 M:月 d:日 h:时 m:分 s:秒 默认：yyyy-MM-dd hh:mm:ss
 * @return {String} 刚刚 | 分钟前 | 小时前 | 1天前
 */
const formatTime = (time, fmt) => {
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  return formatDate(d, fmt)
}

/**
 * 对象深拷贝
 * @param {Object|Array} data Object | Array
 * @return {Object|Array} Object | Array
 */
const deepClone = data => {
  let type = getObjType(data)
  let obj
  if (type === 'Array') {
    obj = []
  } else if (type === 'Object') {
    obj = {}
  } else {
    // 不再具有下一层次
    return data
  }
  if (type === 'Array') {
    for (let i = 0, len = data.length; i < len; i++) {
      obj.push(deepClone(data[i]))
    }
  } else if (type === 'Object') {
    for (let key in data) {
      obj[key] = deepClone(data[key])
    }
  }
  return obj
}


/**
 * 判断传入参数的类型，以字符串的形式返回
 * @param {*} data data
 * @return {String} 基本数据类型 String、Number、Boolean、undefined、Null
 */
const getObjType = data => {
  if (data === null) return "Null";
  if (data === undefined) return "Undefined";
  return Object.prototype.toString.call(data).slice(8, -1);
};


/**
 * 处理对象参数值，排除对象参数值为""、null、undefined，并返回一个新对象
 * @param {Object} obj 对象
 * @return {Object} 排除 ""、null、undefined 的新对象
 */
const delObjectValueNull = obj => {
  let param = {};
  if (obj === null || obj === undefined || obj === "" || getObjType(obj) === "Array") return param;
  for (let key in obj) {
    if (getObjType(obj[key]) === "Object") {
      param[key] = delObjectValueNull(obj[key]);
    } else if (obj[key] !== null && obj[key] !== undefined && obj[key] !== "") {
      param[key] = obj[key];
    }
  }
  return param;
};


/**
 * Object 转 url 查询参
 * @param {Object} obj 对象参数，不能嵌套 
 * @return {String} ?key=val
 */
const toQueryPair = (obj) => {
  if (typeof obj !== 'object') {
    return ''
  }
  let queryParams = "";
  Object.getOwnPropertyNames(obj).forEach(key => {
    queryParams = `${queryParams}&${key}=${obj[key]}`;
  });
  return queryParams.replace(/&/, "?");
}


/**
 * 计算时间差
 * @param {Date} startTime 开始时间
 * @param {Date} endTime 结束时间
 * @return {Array} [days, hours, minutes, seconds]
 */
const timeDifference = (startTime, endTime) => {
  let date1 = startTime || new Date();  //开始时间
  let date2 = endTime || new Date();    //结束时间
  let date3 = date2.getTime() - new Date(date1).getTime();   //时间差的毫秒数    
  //计算出相差天数
  let days = Math.floor(date3 / (24 * 3600 * 1000))
  //计算出小时数
  let leave1 = date3 % (24 * 3600 * 1000)    //计算天数后剩余的毫秒数
  let hours = Math.floor(leave1 / (3600 * 1000))
  //计算相差分钟数
  let leave2 = leave1 % (3600 * 1000)        //计算小时数后剩余的毫秒数
  let minutes = Math.floor(leave2 / (60 * 1000))
  //计算相差秒数
  let leave3 = leave2 % (60 * 1000)      //计算分钟数后剩余的毫秒数
  let seconds = Math.round(leave3 / 1000)
  return [days, hours, minutes, seconds]
}



/**
 * 全屏切换
 */
const fullscreenToggel = () => {
  if (fullscreenEnable()) {
    exitFullScreen()
  } else {
    reqFullScreen()
  }
}
/**
 * esc监听全屏
 * @param {Function} cb 回调函数
 */
const listenfullscreen = (callback) => {
  function listen() {
    callback()
  }

  document.addEventListener('fullscreenchange', function () {
    listen()
  })
  document.addEventListener('mozfullscreenchange', function () {
    listen()
  })
  document.addEventListener('webkitfullscreenchange', function () {
    listen()
  })
  document.addEventListener('msfullscreenchange', function () {
    listen()
  })
}
/**
 * 浏览器判断是否全屏
 * @return {Boolean} Boolean
 */
const fullscreenEnable = () => {
  return document.isFullScreen || document.mozIsFullScreen || document.webkitIsFullScreen
}

/**
 * 浏览器全屏
 */
const reqFullScreen = () => {
  if (document.documentElement.requestFullScreen) {
    document.documentElement.requestFullScreen()
  } else if (document.documentElement.webkitRequestFullScreen) {
    document.documentElement.webkitRequestFullScreen()
  } else if (document.documentElement.mozRequestFullScreen) {
    document.documentElement.mozRequestFullScreen()
  }
}
/**
 * 浏览器退出全屏
 */
const exitFullScreen = () => {
  if (document.documentElement.requestFullScreen) {
    document.exitFullScreen()
  } else if (document.documentElement.webkitRequestFullScreen) {
    document.webkitCancelFullScreen()
  } else if (document.documentElement.mozRequestFullScreen) {
    document.mozCancelFullScreen()
  }
}



/**
 * 生成随机len位数字
 * @param {Number} len 随机数长度
 * @param {Boolean} date 是否 + 时间戳
 * @return {String} 随机数
 */
const randomLenNum = (len, date) => {
  let random = ''
  random = Math.ceil(Math.random() * 100000000000000).toString().substr(0, len || 4)
  if (date) random = random + Date.now()
  return random
}
/**
 * 浏览器打开小窗口
 * @param {String} url 链接地址
 * @param {String} title 窗口标题 
 * @param {String} w width 
 * @param {String} h height 
 */
const openWindow = (url, title, w, h) => {
  // Fixes dual-screen position                            Most browsers       Firefox
  const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left
  const dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top

  const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width
  const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height

  const left = ((width / 2) - (w / 2)) + dualScreenLeft
  const top = ((height / 2) - (h / 2)) + dualScreenTop
  const newWindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left)

  // Puts focus on the newWindow
  if (window.focus) {
    newWindow.focus()
  }
}

/**
 * 日期时间格式化
 * @param {*} oldDate 日期时间
 * @param {String} fmt 格式 y:年 M:月 d:日 h:时 m:分 s:秒 默认：yyyy-MM-dd hh:mm:ss
 * @return {String} formated date
 */
const formatDate = (oldDate, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  let date = new Date()
  if (typeof oldDate === 'string') {
    date = new Date(oldDate.replace(/\-/g, '/'))
  } else if (typeof oldDate === 'number') {
    date = new Date(oldDate)
  } else {
    date = oldDate
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  let o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  function padLeftZero(str) {
    return ('00' + str).substr(str.length)
  }
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
    }
  }
  return fmt
}

/**
 * 判断浏览器环境 
 * 安卓 & 苹果 & ipad & 微信 & 支付宝 & 是否是手机端。
 */
export const BrowserInfo = {
  // isAndroid: Boolean(navigator.userAgent.match(/android/ig)),
  // isIphone: Boolean(navigator.userAgent.match(/iphone|ipod/ig)),
  // isIpad: Boolean(navigator.userAgent.match(/ipad/ig)),
  // isWeixin: Boolean(navigator.userAgent.match(/MicroMessenger/ig)),
  // isAli: Boolean(navigator.userAgent.match(/AlipayClient/ig)),
  // isPhone: Boolean(/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent))
}

/**
 * 函数防抖
 * @param {Function} callback 函数
 * @param {Number} timeout 等待时间
 * @param {Boolean} immediate 决定触发时机
 * @return {Function}
 */
const debounce = (callback, timeout, immediate) => {
  let timer;
  return function () {
    const context = this; // 持有执行上下文
    const args = arguments; // 记录传参
    const later = function () {
      timer = null; // 贤者时间过了，重振旗鼓，重置为初始状态
      if (!immediate) callback.apply(context, args); // 设置为尾部调用才延时触发
    }
    const callNow = immediate && !timer; // 如果确认允许首部调用，且首次调用，那么本次立即触发
    clearTimeout(timer); // 杀掉上次的计时器，重新计时
    timer = setTimeout(later, timeout); // 重启一个计时器，过了贤者时间之后才触发
    callNow && callback.apply(context, args); // 设置为首部调用立即触发
  }
}

/**
 * 函数节流
 * @param {Function} func 函数
 * @param {Number} wait 等待时间
 * @return {Function}
 */
const throttle = (func, wait) => {
  let previous = 0;
  return function () {
    let now = Date.now();
    let context = this;
    let args = arguments;
    if (now - previous > wait) {
      func.apply(context, args);
      previous = now;
    }
  }
}

/**
 * JS获取url参数
 * @param {String} variable key
 * @return {String}
 */
const getQueryVariable = (variable) => {
  const query = window.location.search.substring(1);
  const vars = query.split("&");
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split("=");
    if (pair[0] == variable) { return pair[1]; }
  }
  return (false);
}

/**
 * 获取Url参数，返回一个对象
 * @return {Object}
 */
const getQueryParams = () => {
  const url = document.location.toString();
  let arrObj = url.split("?");
  let params = Object.create(null)
  if (arrObj.length > 1) {
    arrObj = arrObj[1].split("&");
    arrObj.forEach(item => {
      item = item.split("=");
      params[item[0]] = item[1]
    })
  }
  return params;
}
