
/**
 * 上传图片
 * @param {Number} count 单次可选数量
 * @param {Array<String>} sizeType original 原图，compressed 压缩图，默认二者都有
 * @param {Object} [config] 
 * @param {string} [config.uploadUrl]  上传地址
 * @param {string} [config.token]  token
 */
const uploadPics = (count = 1, sizeType, config) => {
  let requestPicUrlList = []
  return new Promise((resolve, reject) => {
    uni.chooseImage({
      count,
      sizeType,
      success: chooseImageRes => {
        uni.showLoading({
          title: '上传中...'
        })
        const tempFilePaths = chooseImageRes.tempFilePaths;
        tempFilePaths.map(item => {
          uploadFile(item, config).then(url => {
            requestPicUrlList.push(url)
            if (tempFilePaths.length === requestPicUrlList.length) {
              uni.hideLoading()
              resolve(requestPicUrlList)
            }
          }).catch(() => {
            reject()
          });
        });
      }, fail: () => {
        reject()
      }
    });
  })
}

const uploadFile = (filePath, config) => {
  return new Promise((resolve, reject) => {
    const { uploadUrl, token } = config
    uni.uploadFile({
      url: uploadUrl, //仅为示例，非真实的接口地址
      filePath,
      header: {
        token
      },
      name: 'file',
      success: uploadFileRes => {
        console.log("🚀 ~ file: index.js ~ line 50 ~ returnnewPromise ~ uploadFileRes", uploadFileRes)
        if (uploadFileRes.statusCode === 200) {
          resolve(JSON.parse(uploadFileRes.data).data)
        } else {
          uni.hideLoading()
          uni.showToast({ title: 'statusCode ' + uploadFileRes.statusCode, icon: 'none' })
          reject()
        }
      },
      fail: err => {
        uni.showToast({
          title: err.data.message || err,
          icon: 'none'
        });
        reject()
      }
    });
  })

}
export { uploadPics, }