import * as utils from "./js/utils";
import * as validate from "./js/validate";

export default { utils, validate }