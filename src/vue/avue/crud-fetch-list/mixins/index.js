/**
 * avue method mixins 
 * 
 * @template 模板语法
 * <avue-crud
      :page.sync="page"
      v-bind="bindVal"
      v-on="onEvent"
    ></avue-crud>
 * 
 * @import 引入路径
 * import CrudFetchList from "@/mixins/CrudFetchList";
 * 
 * @data exports data
 * data() {
    return {
      // 列表接口
      addObj,
      delObj,
      updateObj,
      fetchList,
      // 表格数据
      tableData: [],
      // 表格配置
      tableOption: {
        align: "center",
        menuAlign: "center",
        column: [
          {
            label: "编码",
            prop: "code",
          },
        ],
      },
    };
  },
 * 
 */
export default {
  data() {
    return {
      page: {
        total: 0, // 总页数
        currentPage: 1, // 当前页数
        pageSize: 10 // 每页显示多少条
      },
      searchForm: {},
      tableLoading: false,
      tableData: [],
      otherParams: {},
    }
  },
  computed: {
    bindVal() {
      return {
        'table-loading': this.tableLoading,
        data: this.tableData,
        option: this.tableOption,
      }
    },
    onEvent() {
      return {
        'on-load': this.getList,
        'size-change': this.sizeChange,
        'refresh-change': this.refreshChange,
        'search-change': this.searchChange,
        'current-change': this.currentChange,
        'search-reset': this.clearSearchForm,
      }
    }
  },
  methods: {
    //#region 增删改查
    /**
     * 新增
     * @param {Object} row 行数据
     * @param {callback} done 成功结束回调
     * @param {callback} loading 取消加载回调
     */
    rowSave(row, done, loading) {
      this.addObj(row).then(() => {
        this.clearSearchForm()
        done()
        this.$message({
          message: '保存成功',
          type: 'success',
          showClose: false,
        });
      }).catch(() => {
        loading()
      })
    },
    /**
     * 删除
     * @param {Object} row 选中行
     * @param {Number} index 行序号
     */
    rowDel(row, index) {
      this.$confirm(`确定删除“${row.name || ''}”吗？`, '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
        callback: action => {
          if (action === 'confirm') {
            this.tableLoading = true
            this.delObj(row.id).then(() => {
              this.clearSearchForm()
              this.tableLoading = false
              this.$message({
                message: '删除成功',
                type: 'success',
                showClose: false,
              });
            })
          }
        }
      });
    },
    /**
     * 修改
     * @param {Object} row 行数据
     * @param {Number} index 行序号
     * @param {callback} done 成功结束回调
     * @param {callback} loading 取消加载回调
     */
    rowUpdate(row, index, done, loading) {
      this.updateObj(row).then(() => {
        this.clearSearchForm()
        done()
        this.$message({
          message: '修改成功',
          type: 'success',
          showClose: false,
        });
      }).catch(() => {
        loading()
      })
    },
    /**
     * 查询
     * @param {Object} page 分页信息
     * @param {Object} params 其他参数
     */
    getList(page, params) {
      this.tableLoading = true;
      this.fetchList(
        Object.assign(
          {
            page: page.currentPage,
            limit: page.pageSize
          },
          params,
          this.otherParams,
          this.searchForm
        )
      ).then(response => {
        this.tableLoading = false;
        this.tableData = this.isArray(response.data) ? response.data : response.data.list || response.data.page.list;
        this.page.total = this.isArray(response.data) ? response.data.length : response.data.totalRow || response.data.page.totalRow;
        this.getListOver ? this.getListOver() : '';
        this.calcMaxHeight()
      });
    },
    //#endregion
    isArray(value) {
      return Object.prototype.toString.call(value) === '[object Array]'
    },
    /**
     * 计算table最大高度
     * @param {Number} lessHeight 额外减去高度
     */
    calcMaxHeight(lessHeight = 210) {
      this.$nextTick(() => {
        if (this.$refs.container) {
          const containerHeight = this.$refs.container.$el.scrollHeight
          if (this.tableOption && this.tableOption.maxHeight !== containerHeight) {
            this.$set(
              this.tableOption,
              "maxHeight",
              this.$refs.container.$el.scrollHeight - lessHeight
            );
          }
        }
      });
    },
    clearSearchForm() {
      this.page.currentPage = 1;
      this.page.pageSize = 10;
      this.searchForm = {};
      this.getList(this.page);
    },

    searchChange(form, done) {
      this.page.currentPage = 1;
      this.searchForm = form;
      this.getList(this.page, form);
      done();
    },
    sizeChange(pageSize) {
      this.page.pageSize = pageSize;
    },
    currentChange(current) {
      this.page.currentPage = current;
    },
    toFirstPage() {
      this.page.currentPage = 1;
      this.getList(this.page);
    },
    refreshChange() {
      this.getList(this.page);
    }
  }
}
