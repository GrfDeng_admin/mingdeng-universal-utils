/**
 * 获取分页列表
 * @remark 分页api方法名：fetchListApi
 * 
 * @remark pages.json
 * 
 */
export default {
  data() {
    return {
      pageInfo: {
        currentPage: 1,
        pageSize: 10
      },
      hasMore: true,
      loading: false,
      list: []
    }
  },
  onPullDownRefresh() {
    this.refresh();
  },
  onReachBottom() {
    this.nextPage();
  },
  methods: {
    getList() {
      this.fetchListApi(this.pageInfo.currentPage, this.pageInfo.pageSize, this.otherQueryParams || {}).then(({ data }) => {
        uni.stopPullDownRefresh();
        if (this.selection) {
          data.items.map(item => {
            this.list.push({
              ...item,
              checked: false
            })
          })
        } else {
          this.list = this.list.concat(data.items);
        }
        if (
          this.pageInfo.pageSize * this.pageInfo.currentPage >=
          data.totalCount
        ) {
          this.hasMore = false;
        }
        this.loading = false;
      })
    },
    nextPage() {
      if (this.hasMore && !this.loading) {
        this.pageInfo.currentPage += 1;
        this.getList();
      }
    },
    refresh() {
      this.pageInfo.currentPage = 1;
      this.list = [];
      this.hasMore = true;
      this.getList();
    },
  },
}